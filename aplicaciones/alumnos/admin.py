# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from aplicaciones.alumnos.models import Alumno #importamos el modelo de alumnos para poder verlo en el admin de django.

# REGISTRAR LOS MODELOS CREADOS EN LAS APLICACIONES
admin.site.register(Alumno)
