# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-03 04:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Alumno',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=60)),
                ('apellido', models.CharField(max_length=60)),
                ('edad', models.IntegerField(blank=True, null=True)),
                ('correo_electronico', models.EmailField(max_length=254)),
                ('fecha_inscripcion', models.DateTimeField()),
            ],
        ),
    ]
