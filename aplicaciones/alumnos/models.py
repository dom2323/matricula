# -*- coding: utf-8 -*-
from __future__ import absolute_import
from django.utils import *
from django.db import models
from datetime import datetime

#importar el modelo docente para hacer la relacion de uno a muchos en este modelo
#  que apunte al de los docentes
from aplicaciones.docentes.models import Docente


class AutoDateTimeField(models.DateTimeField):
    def pre_save(self, model_instance, add):
        return datetime.datetime.now()
# Create your models here.
class Alumno(models.Model):#todo nombre de modelo debe ser un nombre en singular
    #creando los campos del modelo

    nombre = models.CharField(blank=False, max_length=60)
    apellido = models.CharField(blank=False, max_length=60)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    fecha_inscripcion = models.DateField(default=timezone.now)
    docente_encargado =  models.ForeignKey(Docente, null= True, blank= True)
    direccion_actual = models.CharField(blank=True, max_length=60,null=True)
    telefono_actual_casa = models.CharField(blank=False, max_length=60, null=True)
    ultimo_centro_de_estudio=models.CharField(blank=False, max_length=60,null=True)
    comentarios = models.CharField(blank=False, max_length=100,null=True)
    def __unicode__(self):#funcion para que se nos devuelva el nombre y no el objeto
	#que hemos registrado
		return '{}'.format(self.nombre)
    # def` get_full(self):
        #hacer consulta de cuantos registros hay con querysetfilter count y validar for
        #mularios con js
        #
        #
        #
        #
