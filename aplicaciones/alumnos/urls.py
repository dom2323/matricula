#Urls de la aplicacion Alumno
from django.conf.urls import url,include
from django.contrib.auth.decorators import login_required
from aplicaciones.alumnos.views import index_alumnos,AlumnoListView,AlumnoCreateView,\
AlumnoUpdateView,AlumnoDeleteView,AlumnoDetailView
#barra invertida para salto de linea
urlpatterns = [
    url(r'^index$', login_required(index_alumnos), name='index_alumnos'),# el signo $ indica que es una exprecion regular, cadena vacia junto con el signo ^.
    url(r'^nuevo$', login_required(AlumnoCreateView.as_view()), name='nuevo'),
    url(r'^listado$', login_required(AlumnoListView.as_view()), name='listado'),#pasando la clases
    url(r'^detalles/(?P<pk>\d+)/$', login_required(AlumnoDetailView.as_view()), name='detalles'),
    #de la vista generica como vista, tambiense se puede colocar un contexto de NOMBRE
    #desde la clase con context_object_name
    url(r'^editar/(?P<pk>\d+)/$', login_required(AlumnoUpdateView.as_view()), name='editar'),# para acceder
    #a cada id de los objetos con las  vstas genericas es necesario usar
    # el parametro pk (primary key)
    url(r'^borrar/(?P<pk>\d+)/$', login_required(AlumnoDeleteView.as_view()), name='borrar'),

]
