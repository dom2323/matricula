# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from aplicaciones.docentes.models import Docente,Secciones #importamos el modelo docente, para verlo en el admin de django

# REGISTRAR LOS MODELOS CREADOS EN LAS APLICACIONES
admin.site.register(Docente)
admin.site.register(Secciones)
