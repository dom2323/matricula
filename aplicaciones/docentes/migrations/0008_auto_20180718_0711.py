# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-18 07:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('docentes', '0007_secciones_color'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='secciones',
            name='color',
        ),
        migrations.AddField(
            model_name='secciones',
            name='turno',
            field=models.CharField(choices=[(b'manana', b'manana'), (b'tarde', b'TARDE'), (b'pendiente', b'FIN DE SEMANA')], default=b'manana', max_length=10),
        ),
    ]
