# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-18 07:37
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('docentes', '0008_auto_20180718_0711'),
    ]

    operations = [
        migrations.AlterField(
            model_name='docente',
            name='seccion_cargo',
            field=models.ForeignKey(blank=True, max_length=15, null=True, on_delete=django.db.models.deletion.CASCADE, to='docentes.Secciones'),
        ),
    ]
