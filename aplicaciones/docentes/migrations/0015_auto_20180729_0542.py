# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-29 05:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('docentes', '0014_docente_dui_docente'),
    ]

    operations = [
        migrations.AlterField(
            model_name='docente',
            name='dui_docente',
            field=models.CharField(default=b'', help_text=b'Ingrese numero de DUI sin guiones', max_length=30),
        ),
    ]
