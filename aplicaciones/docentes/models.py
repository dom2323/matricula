# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models



class Secciones(models.Model):

    opciones_turnos = (
        ('0', ''),
        ('MATUTINO', 'MATUTITO'),
        ('TARDE', 'TARDE'),
        ('FIN DE SEMANA', 'FIN DE SEMANA')

    )
    #crear campo de a;o de estudio usar un count
    turno = models.CharField(max_length=40, choices=opciones_turnos,default='MATUTITO')
    nombre_seccion= models.CharField(blank=False,max_length=30)
    cupor_por_seccion = models.IntegerField(blank=True, null=True)

    def __unicode__(self):#funcion para que se nos devuelva el nombre y no el objeto
	#que hemos registrado
		return '{}'.format(self.nombre_seccion)

# Create your models here.
class Docente(models.Model):
    #campos de el modelo del docente o atributos que comtendra la tabla
    # de los docentes
    dui_docente =  models.CharField(blank=False, max_length=30,default='')
    nombre = models.CharField(blank=False, max_length=30)
    apellido = models.CharField(blank=False, max_length=30)
    nip_docente=models.CharField(blank=False, max_length=20,null=True)
    seccion_cargo = models.ForeignKey(Secciones,null= True, blank= True,max_length=15)
    #corregir este campo mas tarde


    #revisa si el docente puede tener mas de una seccion a cargo
    def __unicode__(self):#funcion para que se nos devuelva el nombre y no el objeto
	#que hemos registrado
		return '{}'.format(self.nombre)
