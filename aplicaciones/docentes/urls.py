#Urls de la aplicacion docente
from django.conf.urls import url,include
from django.contrib.auth.decorators import login_required
from aplicaciones.docentes.views import index_docentes,index_secciones,DocenteCreateView,\
DocenteListView,DocenteUpdateView,DocenteDeleteView,SeccionesCreateView,SeccionesListView

urlpatterns = [
    url(r'^index$', login_required(index_docentes) , name='index_docentes'),# el signo $ indica que es una exprecion regular, cadena vacia junto con el signo ^.
    url(r'^nuevo$', login_required(DocenteCreateView.as_view()), name='nuevo'),
    url(r'^listado$', login_required(DocenteListView.as_view()), name='listado'),
    url(r'^editar/(?P<pk>\d+)/$', login_required(DocenteUpdateView.as_view()), name='editar'),
    url(r'^borrar/(?P<pk>\d+)/$', login_required(DocenteDeleteView.as_view()), name='borrar'),
    ###### URLS DE LAS SECCIONES ######
    url(r'^index_secciones$',login_required(index_secciones) , name='index_secciones'),
    url(r'^nuevas_secciones$', login_required(SeccionesCreateView.as_view()), name='nuevas_secciones'),
    url(r'^listados_secciones$', login_required(SeccionesListView.as_view()), name='listados_secciones'),


]
