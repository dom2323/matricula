# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView,CreateView,UpdateView,DeleteView
from aplicaciones.docentes.forms import Docente_Form,Secciones_Form
from aplicaciones.docentes.models import Docente,Secciones

#vista principal docentes
def index_docentes(request):
    return render(request,'index_principal_docentes.html')



class DocenteListView(ListView):
    model = Docente
    template_name = 'listado_docentes.html'

#vista para crear docentes
#
class DocenteCreateView(CreateView):
    model = Docente
    #indocamos el modelo que en que se basa esta vista con la propiedad model
    form_class = Docente_Form
    #le pasamos el formulario creado a la propiedad form_class
    template_name = 'CRUD_docentes.html'
    #colocamos el formulario que va a usar esta vista con el atributo template_name
    success_url = reverse_lazy('docentes:index_docentes')
    #con success_url redirigimos luego de guardar nuestros registros
    #
    #
class DocenteUpdateView(UpdateView):
    model = Docente#indicando el nombre del modelo a usar
    form_class = Docente_Form#atributo form class se usa
    # para guardar el formulario que hemos creado
    template_name = 'CRUD_docentes.html'#se coloca el templateview
    #que se va a utilizar
    success_url = reverse_lazy('docentes:index_docentes')#indicando la url
    #a la que volvera luego de guardado el objeto
    #
    #
class DocenteDeleteView(DeleteView):
    model = Docente
    template_name = 'eliminar_docentes.html'
    success_url =reverse_lazy('docentes:listado')

###################vistas de las secciones############################

def index_secciones(request):
    return render(request,'index_principal_secciones.html')

class SeccionesListView(ListView):
    model = Secciones
    template_name = 'listado_secciones.html'

class SeccionesCreateView(CreateView):
    model = Secciones
    form_class = Secciones_Form
    template_name = 'CRUD_secciones.html'
    success_url = reverse_lazy('docentes:listados_secciones')
