# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from aplicaciones.encargados.models import Encargado

# REGISTRAR LOS MODELOS CREADOS EN LAS APLICACIONES
admin.site.register(Encargado)
