# -*- coding: utf-8 -*-
from __future__ import absolute_import
from django.utils import *
from django.db import models
from aplicaciones.alumnos.models import Alumno



class Encargado(models.Model):

    #DATOS ENCARGADO
    dui = models.CharField(blank=True, max_length=100,help_text="Ingrese numero de DUI sin guiones")
    alumno = models.OneToOneField(Alumno,null=True, blank=True)#alumno que tiene este encargado
    nombre_encargado =  models.CharField(null= True, blank= True,max_length=60,help_text="Ingrese un nombre valido")
    apellido_encargado = models.CharField(blank=False, max_length=60)
    fecha_nacimiento_encargado = models.DateField(blank=True, null=True)



#Datos PADRES DEL Alumno


    nombre_madre = models.CharField(null= True, blank= True,max_length=60)
    dui_madre = models.CharField(null= True, blank= True,max_length=60)
    ocupacion_madre = models.CharField(null= True, blank= True,max_length=60)
    nombre_padre = models.CharField(null=True,blank=True,max_length=60)
    dui_padre = models.CharField(null= True, blank= True,max_length=60)
    ocupacion_padre = models.CharField(null= True, blank= True,max_length=60)
    def __unicode__(self):
		return '{}'.format(self.nombre_encargado  )
