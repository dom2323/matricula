#Urls de la aplicacion docente
from django.conf.urls import url,include
from django.contrib.auth.decorators import login_required
from aplicaciones.encargados.views import index_encargados,EncargadoListView,EncargadoCreateView
urlpatterns = [
    url(r'^index$', login_required(index_encargados) , name='index_encargados'),# el signo $ indica que es una exprecion regular, cadena vacia junto con el signo ^.
    url(r'^listado$', login_required(EncargadoListView.as_view()) , name='listado'),# el signo $ indica que es una exprecion regular, cadena vacia junto con el signo ^.
    url(r'^nuevo$', login_required(EncargadoCreateView.as_view()) , name='nuevo'),# el signo $ indica que es una exprecion regular, cadena vacia junto con el signo ^.
    

]
