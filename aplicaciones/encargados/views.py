# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse,HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render,redirect
from django.views.generic import ListView, CreateView
from aplicaciones.alumnos.models import Alumno
from aplicaciones.encargados.models import Encargado
from aplicaciones.alumnos.forms import Alumno_Form
from aplicaciones.encargados.forms import Encargado_Form
# Create your views here.
def index_encargados(request):
    return render(request,'index_principal_encargados.html')


class EncargadoListView(ListView):

    model = Encargado
    template_name = 'listado_encargados.html'
class EncargadoCreateView(CreateView):
    model = Encargado
    form_class = Encargado_Form
    second_form_class = Alumno_Form
    template_name = 'CRUD_encargados.html'
    success_url =reverse_lazy('encargados:listado')

    def get_context_data(self, **kwargs):
        context =super(EncargadoCreateView, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form']  = self.form_class(self.request.GET)
        if 'form2' not in context:
            context['form2'] = self.second_form_class(self.request.GET)
        return  context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        form2 = self.second_form_class(request.POST)
        if form.is_valid() and form2.is_valid():
            encargado = form.save(commit=False)
            encargado.alumno = form2.save()
            encargado.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form, form2=form2))
