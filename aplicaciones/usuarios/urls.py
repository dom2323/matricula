#Urls de la aplicacion docente
from django.conf.urls import url,include
from django.contrib.auth.decorators import login_required
from aplicaciones.usuarios.views import index_usuarios,Registro_usuario,EncargadoListView
urlpatterns = [
    url(r'^index$', login_required(index_usuarios) , name='index_usuarios'),
    url(r'^nuevo$', login_required(Registro_usuario.as_view()) , name='nuevo'),
    url(r'^listado$', login_required(EncargadoListView.as_view()) , name='listado'),




]
