# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib.auth.models import User# usando modelo user que trae django
from django.contrib.auth.forms import UserCreationForm#usuando form que trae django, ya no es
#necesario crear forms.py a menos que se quiera agregar campos como nombre apellido, etc\
from django.views.generic import CreateView,ListView#vista generica basada en clases
from django.core.urlresolvers import reverse_lazy#modulo de volver a una url
from aplicaciones.usuarios.form import Registro_form

# Create your views here.
def index_usuarios(request):
    return render(request,'index_principal_usuarios.html')

class Registro_usuario (CreateView):
    model = User
    template_name = 'CRUD_usuarios.html'
    form_class = Registro_form
    success_url = reverse_lazy('usuarios:index_usuarios')

class EncargadoListView(ListView):

    model = User
    template_name = 'listado_usuarios.html'
