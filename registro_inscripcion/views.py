from django.http import HttpResponse
from django.views.generic import View
from django.template.loader import get_template
from aplicaciones.alumnos.models import Alumno
from registro_inscripcion.utils import render_to_pdf

class GeneratePdf(View):
 def get(self, request, *args, **kwargs):
     template=get_template('listado_alumnos.html')
     alumno=Alumno.objects.all()
     context={'alumnos':alumno}
     html=template.render(context)
     pdf= render_to_pdf('pdf/listado_alumnos.html',context)
     return HttpResponse(pdf, content_type='application/pdf')
